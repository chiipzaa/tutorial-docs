# แนะนำการใช้งาน Web API ด้วย Slim Framework
# ![](img/slim.png)

### การติดตั้งใช้งาน

```
composer require slim/slim "^3.0"
```

### การทดสอบการใช้งาน
 - สร้าง file index.php แล้วเพิ่ม code ลงไป

```
<?php
 echo "Hi, Chip";
?>
```
แล้วก็ทดลองเปิดดูผ่าน localhost
