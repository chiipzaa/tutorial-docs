# Laravel
#### reference: https://almsaeedstudio.com/blog/integrate-adminlte-with-laravel

### Integrate AdminLTE with Laravel 5

- ติดตั้ง laravel
> composer global require "laravel/installer=~1.1"

- สร้าง application
> laravel new myapp

- Install AdminLTE via Bower
> cd myapp/public
>
> bower install admin-lte

- การนำไปใช้งาน
