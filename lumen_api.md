# Lumen Api
> Reference: https://lumen.laravel.com/

## ติดตั้งด้วย composer
> composer create-project --prefer-dist laravel/lumen blog

## ติดตั้งด้วย Lumen
> composer global require "laravel/lumen-installer"
>
> lumen new blog

## debug
ถ้าพบปัญหา
> CLI has stopped working.

ให้แก้ไขไฟล์ php.ini เอา ; ออก
> zend_extension=php_opcache.dll

## Example
- https://coderexample.com/restful-api-in-lumen-a-laravel-micro-framework/
- https://lumen.laravel.com/docs/5.3
